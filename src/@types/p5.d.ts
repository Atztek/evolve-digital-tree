interface P5 {
  new (options: any, element: HTMLElement): void;
  setup: Function;
  draw: Function;
  mouseX: number;
  mouseY: number;
  mousePressed: Function;
  _userNode: HTMLElement;
  createCanvas(width: number, heigth: number): void;

  stroke(size: number): void;
  strokeWeight(size: number): void;
  line(startX: number, startY: number, endX: number, endY: number): void;
  rect(startX: number, startY: number, endX: number, endY: number): void;
  fill(style: string): void;
  redraw(): void;
  background(color: number): void;
  background(hex: string): void;
  frameRate(): number;
  noLoop(): void;
  createGraphics(width: number, height: number): P5;
  image(img: P5, x: number, y: number, width?: number, heigth?: number): void;

  loadImage(url: string, callback?: Function): P5;
}

declare module "p5" {}
