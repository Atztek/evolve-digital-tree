import P5 from "p5";
import { Plants, Moss, Tile } from "./Tree";
import { filter } from 'lodash';
import { getRandomColor } from './utils';


const sketch = (world: World) => {
  return (p: P5) => {
    p.setup = function () {
      p.createCanvas(world.widthPx, world.heightPx);
      p.noLoop();
    };
    p.draw = () => {
      world.render();
    };
  };
};

export class World {
  canvas: HTMLElement;
  gridSize = 10;
  _p5: P5;
  gridBufer: P5;
  selectedLayer = 0;

  height: number;
  width = 128;

  worldGrid: Array<Array<Tile>>;

  plants: Array<Plants> = [];

  maxEnergy: number;

  interval = 0;

  get widthPx() {
    return this.width * this.gridSize + 1;
  }

  get heightPx() {
    return this.height * this.gridSize + 1;
  }

  constructor(canvas: HTMLElement, height = 16) {
    this.height = height;
    this.maxEnergy = height + 3;
    this.canvas = canvas;
    this.worldGrid = new Array(this.width).fill(null).map(() => {
      return new Array(this.height);
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    this._p5 = new P5(sketch(this), this.canvas);
    this.gridBufer = this.getGridBufer();

    this.initialPlants();

    this.run();
  }

  /* run simulation */
  run() {
    if (this.interval) this.stop();

    this.interval = setInterval(() => { this.step() }, 100);
  }

  /* stop simulation */
  stop() {
    clearInterval(this.interval);
  }

  /* place initial plants*/
  initialPlants() {
    // initial trees
    for (let i = 15; i < this.width; i += 15) {
      this.addNewPlant(new Moss(this, { x: i, y: this.height - 1 }, getRandomColor()));
    }
  }


  addNewPlant(plant: Plants) {
    this.plants.push(plant);
  }

  render() {
    this._p5.background("#1d2935");
    this._p5.image(this.gridBufer, 0, 0);
    this.renderPlants();
  }

  step() {
    for (const plant of this.plants) {
      plant.collect();
      plant.drain();
      plant.grow();
    }
    this.plants = filter(this.plants, { isDie: false })
    this._p5.redraw();
  }


  /* empty tile by postion */
  emptyTile(position: GridPosition) {
    delete this.worldGrid[position.x][position.y];
  }
  /* get energy by height level */
  getEnergyByLevel(height: number) {
    return this.maxEnergy - height;
  }

  /* count tiles on top by position */
  countTopTiles(position: GridPosition) {
    let multiple = 3;
    for (let y = position.y - 1; y >= 0; y--) {
      if (this.positionIsOcuped({
        x: position.x,
        y: y
      })) {
        multiple--;
        if (multiple == 0) return 0;
      }
    }
    return multiple;
  }

  /* check cell is ocuped */
  positionIsOcuped(position: GridPosition) {
    return !!this.worldGrid[position.x][position.y];
  }

  absPosition(x: number, y: number) {
    if (x < 0) x = this.width + x;
    if (y >= this.height || y < 0) return null;
    return { x: x % this.width, y: y }
  }


  renderPlants() {
    for (const plant of this.plants) {
      plant.render(this._p5, this.gridSize);
    }
  }
  //
  getGridPosition(x: number, y: number): [number, number] {
    return [(x / this.gridSize) | 0, (y / this.gridSize) | 0];
  }

  // grid
  getGridBufer() {
    const grid = this._p5.createGraphics(
      this.canvas.offsetWidth,
      this.canvas.offsetHeight
    );

    for (let x = 0.5; x < this.canvas.offsetWidth; x += this.gridSize) {
      for (let y = 0.5; y < this.canvas.offsetHeight; y += this.gridSize) {
        grid.stroke(0);
        grid.strokeWeight(0.5);
        grid.line(x, 0, x, this.canvas.offsetHeight);
        grid.line(0, y, this.canvas.offsetWidth, y);
      }
    }
    return grid;
  }
}
