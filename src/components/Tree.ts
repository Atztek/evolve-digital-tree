import { World } from './World';
import { random, filter, sumBy, clone } from "lodash";
import { getRandomColor } from './utils';


// left, top, right, bottom
type Gene = [number, number, number, number];

export class Tile {
    position: GridPosition;
    gene: number;
    color: string;
    isSprout = true;
    energy = 0;
    energyToGrow = 18;
    drainEnergy = 13;



    constructor(position: GridPosition, gene = 0, color = "#42b983") {
        this.position = position;
        this.color = color;
        this.gene = gene;
    }

    collectLocal(energy: number) {
        this.energy += energy;
    }

    get mayGrow() {
        return this.energy >= this.energyToGrow;
    }

    render(p5: P5, size: number) {
        if (this.isSprout) {
            p5.fill("#FFFFFF");
        } else {
            p5.fill(this.color);
        }

        p5.rect(this.position.x * size, this.position.y * size, size, size);
    }
}

class BasePlant {
    genome: Array<Gene> = [];
    energy = 400;
    baseColor: string;
    tiles: Array<Tile> = [];

    isDie = false;
    world: World;

    age = 0;
    maxAge: number;

    makeChild(position: GridPosition) {
        return new BasePlant(this.world, position);
    };

    constructor(
        world: World,
        firstTilePosition: GridPosition,
        baseColor = "#42b983",
        maxAge = 80
    ) {
        this.world = world;
        this.baseColor = baseColor;
        this.maxAge = maxAge;
        this.placeTile(new Tile(firstTilePosition, 0, this.baseColor));
    }

    placeTile(tile: Tile) {
        this.world.worldGrid[tile.position.x][tile.position.y] = tile;
        this.tiles.push(tile);
    }

    // if tree is die, clear tiles on world
    die(full: boolean) {
        this.isDie = true;
        const seeds = [];

        for (const tile of this.tiles) {
            this.world.emptyTile(tile.position);
            if (tile.isSprout) {
                seeds.push(tile.position.x);
            }
        }

        if (!full) {
            for (const seed of seeds) {
                if (!this.world.positionIsOcuped({ x: seed, y: this.world.height - 1 })) {
                    if (random(0, 100) <= 50) { // вероятность прорастания зерна    
                        this.world.addNewPlant(
                            this.makeChild({ x: seed, y: this.world.height - 1 })
                        );
                    }
                }
            }
        }
    }

    // collect energy for tree
    collect() {
        this.energy += sumBy(this.tiles, (item: Tile) => {
            // половину забирает росток
            const energy = this.world.getEnergyByLevel(item.position.y) * this.world.countTopTiles(item.position);
            if (item.isSprout) {
                item.collectLocal(energy);
                return energy;
            }
            return energy;
        });
    }


    drain() {
        this.energy -= sumBy(this.tiles, "drainEnergy")
        if (this.energy < 0) {
            this.die(this.age <= this.maxAge / 2);
        }

        this.age++;
        if (this.age > this.maxAge) {
            this.die(false);
        }
    }

    // grow sprouts 
    grow() {
        if (this.isDie) return;
        for (const tile of filter(this.tiles, { isSprout: true })) {
            if (!tile.mayGrow) continue;
            const gene = this.genome[tile.gene];
            let isSeed = true;
            for (const gen in gene) {
                const growGene = this.genome[gene[gen]];
                if (growGene) {
                    let newPos = null;
                    switch (gen) {
                        case "0":
                            newPos = this.world.absPosition(tile.position.x - 1, tile.position.y);
                            break;
                        case "1":
                            newPos = this.world.absPosition(tile.position.x, tile.position.y - 1);
                            break;
                        case "2":
                            newPos = this.world.absPosition(tile.position.x + 1, tile.position.y);
                            break;
                        case "3":
                            newPos = this.world.absPosition(tile.position.x, tile.position.y + 1);
                            break;
                    }

                    if (newPos && !this.world.positionIsOcuped(newPos)) {
                        this.placeTile(new Tile(newPos, gene[gen], this.baseColor));
                        isSeed = false;
                    }
                }
            }

            if (!isSeed) {
                tile.isSprout = false;
            }
        }
    }


    render(p5: P5, gridSize: number) {
        for (const tile of this.tiles) {
            tile.render(p5, gridSize);
        }
    }
}

export class Moss extends BasePlant {
    genomeLength = 16;

    constructor(
        world: World,
        firstTilePosition: GridPosition,
        baseColor = "#42b983",
        genome: Array<Gene> = [],
        maxAge = 80
    ) {
        super(world, firstTilePosition, baseColor, maxAge);
        if (genome.length > 0) {
            this.genome = genome;
        } else {
            this.generateInitialGenome();
        }
    }

    makeChild(position: GridPosition) {
        const genome = JSON.parse(JSON.stringify(this.genome));
        if (0 == random(0, 4)) { // 20% шанс мутации одного гена
            genome[random(0, this.genomeLength - 1)][random(0, 3)] = random(0, this.genomeLength * 2);
        }

        return new Moss(this.world, position, getRandomColor(), genome);
    };

    generateInitialGenome() {
        for (let i = 0; i < this.genomeLength; i++) {
            const gene = [0, 0, 0, 0].map((item, i) =>
                random(0, this.genomeLength * 2)
            );
            this.genome.push(gene as Gene);
        }
    }
}

class Tree extends BasePlant { }

export type Plants = Moss | Tree;
